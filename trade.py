from log import Log

COMMISSION_PROC = 0.0025
MIN_PROFIT = 0.09

class Trade(object):
	def __init__(self, currentPrice, tradeType, strategy, units, stopLoss, chart=None):
		self.type = tradeType
		self.strategy = strategy
		self.output = Log()
		self.status = "UNKNOWN"
		self.entryPrice = currentPrice
		self.exitPrice = ""
		self.units = units
		self.originUnits = self.strategy.unitsBalance
		self.originBase = self.strategy.baseBalance
		self.chart = chart
		if self.type == 'bull':
			self.stopLossPrice = currentPrice * (1 - stopLoss)
			self.strategy.unitsBalance += self.units
			self.strategy.baseBalance -= self.units * self.entryPrice * (1 + COMMISSION_PROC)
			if chart:
				if chart.buy(quantity=units, rate=currentPrice):
					self.status = "OPEN"
			else:
				self.status = "OPEN"
		elif self.type == 'bear':
			self.stopLossPrice = currentPrice * (1 + stopLoss)
			self.strategy.unitsBalance -= self.units
			self.bearWorkingBase = self.units * self.entryPrice * (1 - COMMISSION_PROC)
			self.strategy.baseBalance += self.bearWorkingBase
			if chart:
				if chart.sell(quantity=units, rate=currentPrice):
					self.status = "OPEN"
			else:
				self.status = "OPEN"
	
	def close(self, currentPrice):
		self.status = "CLOSED"
		self.exitPrice = currentPrice

		if self.type == 'bull':
			self.strategy.unitsBalance -= self.units
			self.strategy.baseBalance += self.units * self.exitPrice * (1 - COMMISSION_PROC)
			
			# do api trade close
		elif self.type == 'bear':
			baseWithCommission = self.bearWorkingBase * (1 - COMMISSION_PROC)
			self.units = baseWithCommission / self.exitPrice
			self.strategy.unitsBalance += self.units
			self.strategy.baseBalance -= self.bearWorkingBase
			# do api trade close

		self.showTrade()

	def totalGain(self):
		return (self.strategy.unitsBalance - self.originUnits) + (self.strategy.baseBalance - self.originBase) / self.exitPrice

	def isProfitable(self, price):
		entryValue = self.originBase
		currentValue = self.units * price
		commission = currentValue * COMMISSION_PROC
		if self.type == 'bull':
			finalValue = currentValue - commission - MIN_PROFIT
			if entryValue <= finalValue:
				return True
		elif self.type == 'bear':
			finalValue = currentValue + commission + MIN_PROFIT
			if entryValue >= finalValue:
				return True
		return False

	def tick(self, currentPrice):
		if self.type == 'bull':
			if (currentPrice < self.stopLossPrice):
				self.close(currentPrice)
		elif self.type == 'bear':
			if (currentPrice > self.stopLossPrice):
				self.close(currentPrice)

	def showTrade(self, baseProfit=0):

		tradeType = ('\033[92mbull\033[0m' if self.type == 'bull' else '\033[91mbear\033[0m')
		tradeStatus = "Entry Price: "+str(self.entryPrice)+" (" + tradeType + ") Exit Price: "+str(self.exitPrice)

		totalGain = self.totalGain()

		if (self.status == "CLOSED"):
			if totalGain >= 0:
				tradeStatus = tradeStatus + " \033[92m"
			else:
				tradeStatus = tradeStatus + " \033[91m"

			tradeStatus = tradeStatus + str(totalGain) +"\033[0m " + '(units ' + str(self.strategy.unitsBalance) + ', base ' + str(self.strategy.baseBalance)+')'

		self.output.log(tradeStatus)
	