import sys
import sys
from chart import Chart
from candlestick import Candlestick
import time
from strategy import Strategy
import pdb

sleepTime = 10

def main(argv):
    forceBacktest = True
    if forceBacktest:
        backtest()
    else:
        straight()

def straight():
    chart = Chart("bittrex", "USDT-BTG", "hour", backtest=False)
    strategy = Strategy()  
    candlesticks = []
    developingCandlestick = Candlestick()

    while True:
        currentPrice = chart.getCurrentPrice()
        developingCandlestick.tick(currentPrice)

        if (developingCandlestick.isClosed()):
            candlesticks.append(developingCandlestick)
            strategy.tick(developingCandlestick)
            developingCandlestick = Candlestick()
        
        time.sleep(sleepTime)

def backtest():
    print "Performing backtest..."

    chart = Chart("bittrex","USDT-BTG", "hour")
    strategy = Strategy()  
    candlesticks = chart.candlesticks

    for candlestick in candlesticks:
        strategy.tick(candlestick)

    strategy.finished()

if __name__ == "__main__":
    main(sys.argv[1:])