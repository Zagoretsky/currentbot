from log import Log
from indicators import Indicators
from trade import Trade
import matplotlib.pyplot as plt
from matplotlib.finance import candlestick2_ohlc

MA_FAST = 55
MA_SLOW = 110
TRADE_FRACTION = 20
STOP_LOSS = 0.3
PERCENT_MAE = 9
MA_NORM = 50
RSI_PERIOD = 14
MA_CHECK = 200

class Strategy(object):
    def __init__(self, chart=None):
        self.output = Log()
        self.unitsBalance = 25            # request from api
        self.baseBalance = 5000
        self.prices = []
        self.trades = []
        self.currentPrice = ""
        self.currentClose = ""
        self.numSimulTrades = 1
        self.indicators = Indicators()
        self.candlesticks = []
        self.bullSignals = []
        self.bearSignals = []
        self.maFast = []
        self.maSlow = []
        self.maSub = []
        self.maDiff = []
        self.lastTriggeredN = -1
        self.chart = chart

        self.emaFast = []
        self.emaSlow = []
        self.emaSub = []
        self.eemaSub = []
        self.macdDiff = []
        self.maNorm = []
        self.maHigh = []
        self.maLow = []
        self.maCheck = []
        self.RSI = []
        self.TR = []
        self.ATR = []
        self.MAATR = []

    def tick(self, candlestick):
        self.candlesticks.append(candlestick)
        self.currentPrice = float(candlestick.priceAverage)
        self.prices.append(self.currentPrice)
        self.calculateMA()
        self.calculateRSI()
        self.calculateTR()
        self.calculateATR()
        self.calculateMAATR()

        self.evaluatePositions(candlestick)
        self.updateOpenTrades()
        # self.showPositions()

    def calculateTR(self):
        if len(self.candlesticks) >= 2:
            yclose = self.candlesticks[-2].close
            c = self.candlesticks[-1]
            currentTR = self.indicators.TR(c.close, c.high, c.low, c.open, yclose)
            self.TR.append(currentTR)
        else:
            self.TR.append(0) 


    def calculateATR(self):
        currentATR = self.indicators.movingAverage(self.TR, 14)
        self.ATR.append(currentATR)

    def calculateMAATR(self):
        currentMAATR = self.indicators.movingAverage(self.ATR, 100)
        self.MAATR.append(currentMAATR)        

    def calculateMA(self):
        currentMAFast = self.indicators.movingAverage(self.prices, MA_FAST)
        currentMASlow = self.indicators.movingAverage(self.prices, MA_SLOW)
        self.maFast.append(currentMAFast)
        self.maSlow.append(currentMASlow)
        currentMAcheck = self.indicators.movingAverage(self.prices, MA_CHECK)
        self.maCheck.append(currentMAcheck)

 
    def finished(self):
        self.showGraph()

    def availableUnits(self, tradeType, price=None):
        if tradeType == 'bull':
            availableUnits = self.baseBalance / price   # may want to consider commission here
            if availableUnits >= TRADE_FRACTION:
                return TRADE_FRACTION
        elif tradeType == 'bear':   
            if self.unitsBalance >= TRADE_FRACTION:
                return TRADE_FRACTION
        return None

    def calculateRSI(self):
        if len(self.prices) <= 14:
            currentRSI = 50
            self.RSI.append(currentRSI)
        else:    
            currentRSI = self.indicators.RSI(self.prices, RSI_PERIOD)
            self.RSI.append(currentRSI)

    def rsi_signals(self, n):
        valid = len(self.RSI) > 1 and len(self.maFast) > 1
        if valid == False:
            return (False, False)


        minStepsBeforeNextSignalToObserve = 24
        observable = (n - self.lastTriggeredN > minStepsBeforeNextSignalToObserve)


        rsiApprovedBuy = float(self.RSI[-1]) < 22
        minPriceApprovedBuy = self.currentPrice < ((float(min(self.prices[-168:])*1.2)))
        crossover = float(self.maFast[-1]) > float(self.maSlow[-1])
        #higher = float(self.maFast[-1]) > float(self.maSlow[-1])
        #wasLower = float(self.maFast[-2]) < float(self.maSlow[-2])
        #crossoverR = higher and wasLower
        AtrAprovesSell = float(self.ATR[-1]) > 1.6 * float(self.MAATR[-1])
        rsiApprovedSell = float(self.RSI[-1]) > 77
        
          
        bullishSignal = observable and rsiApprovedBuy and crossover and minPriceApprovedBuy 
        bearishSignal = observable and rsiApprovedSell and AtrAprovesSell 
        return (bullishSignal, bearishSignal)


    def evaluatePositions(self, candlestick):
        n = candlestick.n
        openTrades = []
        for trade in self.trades:
            if (trade.status == "OPEN"):
                openTrades.append(trade)

        bullishSignal, bearishSignal = self.rsi_signals(n)

        tradeAllowed = len(openTrades) < self.numSimulTrades
        if tradeAllowed:
            if bullishSignal:
                availableUnits = self.availableUnits('bull', self.currentPrice)
                print 'saw bull signal ' + str(n) + ' ' + ('' if availableUnits else ' (no base units available)' )
                if availableUnits:
                    self.lastTriggeredN = n
                    pendingTrade = Trade(self.currentPrice, stopLoss=STOP_LOSS, tradeType='bull', strategy=self, units=availableUnits)
                    if pendingTrade.status == 'OPEN':
                        self.trades.append(pendingTrade) 
                        self.bullSignals.append(n)
                    else:
                        self.output.log('EXCEPTION!')
            elif bearishSignal:
                availableUnits = self.availableUnits('bear')
                print 'saw bear signal ' + str(n) + ' ' + ('' if availableUnits else ' (no units available)' )
                if availableUnits:
                    self.lastTriggeredN = n
                    pendingTrade = Trade(self.currentPrice, stopLoss=STOP_LOSS, tradeType='bear', strategy=self, units=availableUnits)
                    if pendingTrade.status == 'OPEN':
                        self.trades.append(pendingTrade) 
                        self.bearSignals.append(n)
                    else:
                        self.output.log('EXCEPTION!')

        for trade in openTrades:
            bullCloseSignal = bearishSignal   # close
            bearCloseSignal = bullishSignal
            if trade.type == 'bull':   
                if bullCloseSignal or trade.isProfitable(self.currentPrice):     
                    trade.close(self.currentPrice)
            elif trade.type == 'bear':
                if bearCloseSignal or trade.isProfitable(self.currentPrice):   
                    trade.close(self.currentPrice)

    def updateOpenTrades(self):
        for trade in self.trades:
            if (trade.status == "OPEN"):
                trade.tick(self.currentPrice)

    def showPositions(self):
        for trade in self.trades:
            trade.showTrade()

    def showGraph(self):
        def average(vals):
            return sum(vals) * 1.0 / len(vals)

        candlesticks = self.candlesticks
        opens = [c.open for c in candlesticks]
        highs = [c.high for c in candlesticks]
        lows = [c.low for c in candlesticks]
        closes = [c.close for c in candlesticks]
        fig, ax = plt.subplots()

        for n in self.bearSignals:
            plt.plot(n, self.candlesticks[n].high, 'v', color='r')

        for n in self.bullSignals:
            plt.plot(n, self.candlesticks[n].low, '^', color='g')
            
        candlestick2_ohlc(ax, opens, highs, lows, closes, width=0.6)

        plt.plot(self.maFast, color='blue')
        plt.plot(self.maSlow, color='yellow')
        #plt.plot(self.maCheck,color='mediumturquoise')

            # @@@ RSI @@@ #
        plt.plot(map(lambda t: t*1, self.RSI), color='black', linewidth=0.7)
        plt.axhline(y=5, color='black')
        plt.axhline(y=100, color='black')
        plt.axhline(y=80, color='red')
        plt.axhline(y=20, color='green')

            # @@@ ATR @@@ #
        #plt.plot(map(lambda t: t*6, self.ATR), color='black', linewidth=0.7)
        #plt.plot(map(lambda t: t*6, self.MAATR), color='red', linewidth=0.7)

 
        plt.show()
