from bittrex import Bittrex, API_V2_0
import urllib, json
import pprint
from candlestick import Candlestick
import matplotlib.pyplot as plt
from matplotlib.finance import candlestick2_ohlc
from log import Log

class Chart(object):
    def __init__(self, exchange, pair, period, backtest=True):
        self.pair = pair
        self.period = period
        self.output = Log()
        self.backtest = backtest

        self.candlesticks = []

        if (exchange == "bittrex"):
            self.bittrex1 = Bittrex(None, None)
            self.bittrex2 = Bittrex(None, None, api_version=API_V2_0)
            response = self.bittrex2.get_candles(self.pair, self.period)
            if response:
                candles = response["result"]
                for candlestickDict in candles:
                    n = candles.index(candlestickDict)
                    candlestick = Candlestick(self.periodInSecs(), candlestickDict['O'], candlestickDict['C'], candlestickDict['H'], candlestickDict['L'], candleTime=candlestickDict['T'], n=n)    
                    self.candlesticks.append(candlestick)

    def periodInSecs(self):
        if self.period == 'hour':
            return 3600
        abort(1)

    def getPoints(self):
        return self.data    

    def getCurrentPrice(self):
        result = self.bittrex1.get_ticker(self.pair) ["result"] 
        return result["Last"]

    def buy(self, quantity=None, rate=None):
        if self.backtest:
            return True
        response = self.bittrex2.trade_buy(market=self.pair, order_type='LIMIT', quantity=quantity, rate=rate, time_in_effect='GOOD_TIL_CANCELLED')
        self.output.log(response)
        return response['success']

    def sell(self, quantity=None, rate=None):
        if self.backtest:
            return True
        response = self.bittrex2.trade_sell(market=self.pair, order_type='LIMIT', quantity=quantity, rate=rate, time_in_effect='GOOD_TIL_CANCELLED')
        self.output.log(response)
        return response['success']

    def show(self):
        def average(vals):
            return sum(vals) * 1.0 / len(vals)

        candlesticks = self.candlesticks[-60]
        opens = [c.open for c in candlesticks]
        highs = [c.high for c in candlesticks]      
        lows = [c.high for c in candlesticks]
        closes = [c.close for c in candlesticks]
        fig, ax = plt.subplots()
        candlestick2_ohlc(ax, opens, highs, lows, closes, width=0.6)

    def getCurrentPrice(self):
        result = self.bittrex1.get_ticker(self.pair)["result"]
        return result["Last"]
